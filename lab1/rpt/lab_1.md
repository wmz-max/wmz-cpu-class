## Spike and cosim
**1. Spike model (with risc-v pk) execution correctness: simulate programs atop the proxy kernel**


**report**

    clone spike and run hello.c

![hello.png](./images/1.jpg)

**2.GreenRio core RTL execution correctness: pass some ISA tests (Synopsys VCS environment)**
**report**

    add a dram to the original hehe, and use this dram to simulate the wishbone behaviour, use this dram to load elf(hex),and add a fist.f and use vcs to pass compile and run the add elf. check the tohost signal between the lsu and cache, if monitor the tohost, print pass. 

![dram.png](./images/2.jpg)
![add elf pass.png](./images/3.jpg)

**3.You need to run the elf file on the spike emulator and print the log**

**report**

    use command spike -l add  to print log

![add log.png](./images/4.jpg)

**4.You need to have GreenRio run the risc-v elf in an RTL simulation environment and get the corresponding results: register states, values, etc**

**report**

    I think 4 and 6 are the same tasks, first, add print regs in the rcu and prf when commit a instruction. second, use dpi to send the reg data to the cpp. third, use cpp to handle and print the reg data which this committed instruction change. fourth, after print the log, compare the log from spike and the log from cpp dpi.

![add log.png](./images/5.jpg)
![add log.png](./images/6.jpg)
![add log.png](./images/7.jpg)

**5.Front-end co-sim: use the same ISA/program/elf to compare the results in the decode phase (spike and RTL)**

**report**

    add print regs in the decode, and use dpi to send the data to cpp and then print the log, compare the instruction order from the cpp and the spike, but i only finish the instruction that add elf use. i will finish the others after lab1.

![add log.png](./images/8.jpg)
![add log.png](./images/9.jpg)

**6.Back-end co-sim: compare commit order, register states, memory states, register values, pc values, etc. (spike and RTL)**

**report**

    I think 4 and 6 are the same tasks, first, add print regs in the rcu and prf when commit a instruction. second, use dpi to send the reg data to the cpp. third, use cpp to handle and print the reg data which this committed instruction change. fourth, after print the log, compare the log from spike and the log from cpp dpi.

![add log.png](./images/5.jpg)
![add log.png](./images/6.jpg)
![add log.png](./images/7.jpg)

## Open EDA flow

**report**

    lib1

![add log.png](./images/10.jpg)
![add log.png](./images/11.jpg)

**report**

    lib2

![add log.png](./images/12.jpg)
![add log.png](./images/13.jpg)
