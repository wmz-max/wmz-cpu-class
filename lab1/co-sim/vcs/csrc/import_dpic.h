
 extern void log_decode_print(/* INPUT */int pc, /* INPUT */int ins, /* INPUT */unsigned char valid_in, /* INPUT */unsigned char alu_a_0, /* INPUT */unsigned char alu_a_1, /* INPUT */unsigned char alu_b_0, /* INPUT */unsigned char alu_b_1, /* INPUT */unsigned char half, /* INPUT */unsigned char branch_out_w, /* INPUT */unsigned char jump_out_w, 
/* INPUT */unsigned char lui, /* INPUT */unsigned char slli);

 extern void preg_sync(/* INPUT */unsigned char alu_valid, /* INPUT */unsigned char lsu_valid, /* INPUT */long long alu_data_in, /* INPUT */long long lsu_data_in, /* INPUT */int alu_address, /* INPUT */int lsu_address);

 extern void log_print(/* INPUT */unsigned char co_commit, /* INPUT */int co_pc_in, /* INPUT */unsigned char co_store_in, /* INPUT */unsigned char co_fence, /* INPUT */unsigned char co_mret, /* INPUT */unsigned char co_wfi, /* INPUT */unsigned char co_uses_csr, /* INPUT */int co_rob_rd, /* INPUT */unsigned char co_csr_iss_ctrl, /* INPUT */int co_prf_name, 
/* INPUT */int co_csr_address);

 extern void get_log_handler();

 extern void close_log();
