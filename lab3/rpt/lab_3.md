## cmodel
**1. lab3 cmodel 部分**

    完成了整个hehe1 core uncore 部分的cmodel，目前可以跑过isa-rv64ui里的指令(用cad4/5的环境)。

**2. core 部分提升**   

    现在是one-issue的，所以经常会出现ins-buffer满的情况，core部分的提升我认为最重要的是变成dual-issue的，增加decode和fu.

**3. uncore 部分提升** 

    uncore部分现在最大的问题是，cacheline太短，每次只能取一条指令。
    1 首先把cacheline加长 让fetch一次取上来多条指令
    2 然后将直接映射变为组相连，plru替换算法，进而提升性能
    3 最后可以将mshr深度加大，增加mlfb和ewrq
    4 可以增加多核内容，例如用mesi protocol

## Open EDA flow

**report**

    后端的结果在 RUN_2023.1.1_07.35.16 文件中



