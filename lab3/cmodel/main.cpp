#include <iostream>
#include <cstdio>
#include <string.h>
#include <cmath>
#include<fstream>
using namespace std;
using std::ifstream;

//PARAM
#define Maxcycle 2200
#define BTB_SIZE 4
#define MEM (2 << 20)
#define RESET_VECTOR 0
#define INS_BUFFER_SIZE 9
#define REG_SIZE 40
#define ICACHE_DELAY 2
#define DCACHE_DELAY 3
#define ICACHE_INS_BUFFER_DELAY 1
#define INS_BUFFER_DECODE_DELAY 1
#define ALU_DELAY 1
#define DECODE_RCU_DELAY 1
#define CSR_DELAY 1
#define LSQ_DEPTH 3
#define ROB_DEPTH 5
#define FREE_LIST 39
//struct
struct CHECK_BTB_S
{
    int btb_pc;
    bool btb_hit;
};
struct INS
{
    char ins[32] ;
};
struct ST_B
{
    char ins[8] ;
};
struct ST_H
{
    char ins[16] ;
};
struct ST_W
{
    char ins[32] ;
};
struct ST_D
{
    char ins[64] ;
};
struct ROB_LINE_S
{
    int basic_pc = 0;
    int basic_pc_next = 0;
    bool basic_use_rs1 = 0;
    bool basic_use_rs2 = 0;
    bool basic_use_rd = 0;
    int basic_rs1 = 0;
    int basic_rs2 = 0;
    int basic_rd = 0;
    int basic_rename_rd = 0;
    int basic_last_rename_rd = 0;
    long long basic_rs1_data = 0;
    long long basic_rs2_data = 0;
    bool basic_is_half = 0;
    long long basic_ins = 0;
    bool is_lsu = 0;
    bool lsu_load = 0;
    bool lsu_store = 0;
    int  lsu_load_store_size = 0;
    bool lsu_sign = 0;
    int  lsu_imm = 0;

    bool is_alu = 0;
    bool is_add = 0;
    bool is_sub = 0;
    long long add_sub_alu_1 = 0;
    long long add_sub_alu_2 = 0;

    bool is_fence = 0;

    bool is_jump = 0;
    bool j_b_success = 0;

    bool is_lui = 0;
    long long lui_imm = 0;

    bool is_branch = 0;
    int branch_imm = 0;
    string branch_func = "";
    
    bool is_xor = 0;
    bool is_or = 0;
    bool is_and = 0;
    int op_imm = 0;

    bool is_shift = 0;
    int shift_type = 0;//0sll 1srl 2sra
    int shift = 0;

    bool is_slt = 0;
    int slt_imm = 0;
    int slt_result = 0;

    long long alu_result = 0;

    int rob_index = 0;
    bool finish = 0;

    bool is_mret = 0;
    bool is_ecall = 0;
    bool is_nop = 0;
    bool is_csr = 0;
    bool csr_no_use = 0;

};

//Fuction
string toIvert(string n, int len)
{
    string s;
    for(int i = 0; i < len; i++)
    {
        if(n[i] == '0')
        {
            s = s + '1';
        }
        else
        {
            s = s + '0';
        }
        
    }
    return s;
}

INS toBinary(long long n)
{
    INS ins;
    int i = 0;
    for(int j = 0; j < 32 ;j++)
    {
        ins.ins[j] = '0';
    }
    while (n != 0){
        ins.ins[i] = ( n % 2 == 0 ? '0' : '1' );
        i++;
        n /= 2;
    }
    return ins;
}

void tostring(int pc_cout)
{
    INS cout_pc = toBinary(pc_cout);
    for(int i = 31; i >= 0 ;i--)
    {
        cout << cout_pc.ins[i];
    }
    cout << endl;
}

long long toint(string s, int l)
{
    long long sum = 0;
    for(int i = 0; i < l;i++)
    {
        sum = sum + (s[i]-48) * pow(2,l-i-1);
    }

    return sum;
}



ST_D toBinary_d(long long n)
{
    ST_D ins;
    int i = 0;
    if(n < 0)
    {
        long long m = abs(n);
        long long q = m ^ 0xffffffffffffffff;
        q = q + 1;
        n = q;
    }
    for(int j = 0; j < 64 ;j++)
    {
        ins.ins[j] = '0';
    }
    while (n != 0){
        ins.ins[i] = ( n % 2 == 0 ? '0' : '1' );
        i++;
        n /= 2;
    }
    return ins;
}
ST_B toBinary_b(long long n)
{
    ST_B ins;
    int i = 0;
    if(n < 0)
    {
        long long m = abs(n);
        long long q = m ^ 0xff;
        q = q + 1;
        n = q;
    }
    for(int j = 0; j < 8 ;j++)
    {
        ins.ins[j] = '0';
    }
    while (n != 0){
        ins.ins[i] = ( n % 2 == 0 ? '0' : '1' );
        i++;
        n /= 2;
    }
    return ins;
}
ST_H toBinary_h(long long n)
{
    ST_H ins;
    int i = 0;
    if(n < 0)
    {
        long long m = abs(n);
        long long q = m ^ 0xffff;
        q = q + 1;
        n = q;
    }
    for(int j = 0; j < 16 ;j++)
    {
        ins.ins[j] = '0';
    }
    while (n != 0){
        ins.ins[i] = ( n % 2 == 0 ? '0' : '1' );
        i++;
        n /= 2;
    }
    return ins;
}
ST_W toBinary_w(long long n)
{
    ST_W ins;
    int i = 0;
    if(n < 0)
    {
        long long m = abs(n);
        long long q = m ^ 0xffffffff;
        q = q + 1;
        n = q;
    }
    for(int j = 0; j < 32 ;j++)
    {
        ins.ins[j] = '0';
    }
    while (n != 0){
        ins.ins[i] = ( n % 2 == 0 ? '0' : '1' );
        i++;
        n /= 2;
    }
    return ins;
}

void strmncpy(char* s, int m, int n, char* t)
{
    /*s-指向源字符串,t-指向目标字符串,m-起始位置,n-字符个数*/
    char* p = s;//这个用来跑
    char* q = t;//这个用来存
    int a = 0;//用于执行循环的次数
    p = p + m;//直接把地址改到目标数组的目标位置上去，最简单
    while (a < n)
    {
        *q++ = *p++;//把符合的值赋到用来存的指针这边
        a++;
    }
    *q = '\0';//结束，输出*q
}
//Global
long long cycle;

//MEM
char mem[MEM][8];
bool icache_req_ready;
bool icache_resp_valid;

//FREE_LIST
int free_list[FREE_LIST];
int free_list_head = 0;
int free_list_tail = 0;
//RENAME
int rename_rd[REG_SIZE];
//ROB
ROB_LINE_S rob[ROB_DEPTH];
bool rob_full;
bool rob_empty;
int rob_head_commit;
int rob_head_iss;
int rob_tail;
//ISS
bool iss_alu;
bool iss_lsu;
bool iss_csr;
bool commit_directly;
//Fetch
int pc = 0;
int pc_next = 0;

////BTB
int btb_pc[BTB_SIZE];
int btb_pc_next[BTB_SIZE];

////INSBUFFER
int ins_buffer_pc[INS_BUFFER_SIZE];
long long ins_buffer_ins[INS_BUFFER_SIZE];
bool ins_buffer_full;
bool ins_buffer_empty;
int ins_buffer_head;
int ins_buffer_tail;

//PRF
long long registers[REG_SIZE];
int rs1_read_addr;
int rs2_read_addr;
int alu_write_prf_addr;
int lsu_write_prf_addr;
long long alu_write_prf_data;
long long lsu_write_prf_data;
bool registers_p[REG_SIZE];
int commit_update_prf;

//Decode
bool decode_receive_ins_ready;
int ins_buffer_to_decode_pc;
long long ins_buffer_to_decode_ins;
long long decode_to_rcu_cycle;
//CSR
long long mepc = 0;
long long mtvec = 0;
int mcause = 8;
long long csr_current_cycle;
bool csr_req_ready = 0;

long long Read_PRF_data(int addr)
{
    return registers[addr];
}

bool Read_PRF_P(int addr)
{
    return registers_p[addr];
}

////////////////LSU///////////////////
bool dcache_req_ready;
long long dcache_current_cycle;
int addr;
long long load_data;
ROB_LINE_S lsq;
void LSQ_SEND_TO_DCACHE()
{
    dcache_req_ready = 0;
    dcache_current_cycle = cycle;
    if(lsq.basic_use_rs1)
    {
        lsq.basic_rs1_data = Read_PRF_data(rename_rd[lsq.basic_rs1]);
    }
    if(lsq.basic_use_rs2)
    {
        lsq.basic_rs2_data = Read_PRF_data(rename_rd[lsq.basic_rs2]);
    }
    addr = lsq.basic_rs1_data + lsq.lsu_imm;
    string load_data_s;
    
    if(lsq.lsu_load)//0 1 2 3 4 5 6 7
    {
        if(lsq.lsu_load_store_size == 1 & lsq.lsu_sign)
        {
            for(int i = 1; i <= 7;i++)
            {
                string s(1,mem[addr][i]);
                load_data_s = load_data_s + s;
            }
            //load_data = (-1) * (mem[addr][0]-48) * pow(2,7) + toint(load_data_s,7);
            if(mem[addr][0] == '1')
            {
                load_data_s = toIvert(load_data_s,7);
                load_data = -(toint(load_data_s,7) + 1);
            }
            else
            {
                load_data = toint(load_data_s,7);
            }
        }
        if(lsq.lsu_load_store_size == 2 & lsq.lsu_sign)
        {
            for(int j = 0 ; j < 1 ;j++)
            {
                for(int i = 0; i <= 7 ;i++)
                {
                    string s(1,mem[addr + j][i]);
                    load_data_s = load_data_s + s;
                }
            }
            for(int i = 1; i <= 7;i++)
            {
                string s(1,mem[addr+1][i]);
                load_data_s = load_data_s + s;
            }
            //load_data = (-1) * (mem[addr+1][0]-48) * pow(2,15) + toint(load_data_s,15);
            if(mem[addr+1][0] == '1')
            {
                load_data_s = toIvert(load_data_s,15);
                load_data = -(toint(load_data_s,15) + 1);
            }
            else
            {
                load_data = toint(load_data_s,15);
            }
        }
        if(lsq.lsu_load_store_size == 4 & lsq.lsu_sign)
        {
            for(int j = 0 ; j < 3 ;j++)
            {
                for(int i = 0; i <= 7 ;i++)
                {
                    string s(1,mem[addr + j][i]);
                    load_data_s = load_data_s + s;
                }
            }
            for(int i = 1; i <= 7;i++)
            {
                string s(1,mem[addr+3][i]);
                load_data_s = load_data_s + s;
            }
            //load_data = (-1) * (mem[addr+3][0]-48) * pow(2,31) + toint(load_data_s,31);
            if(mem[addr+3][0] == '1')
            {
                load_data_s = toIvert(load_data_s,31);
                load_data = -(toint(load_data_s,31) + 1);
            }
            else
            {
                load_data = toint(load_data_s,31);
            }
        }
        if(lsq.lsu_load_store_size == 8 & lsq.lsu_sign)
        {
            for(int j = 0 ; j < 7 ;j++)
            {
                for(int i = 0; i <= 7 ;i++)
                {
                    string s(1,mem[addr + j][i]);
                    load_data_s = load_data_s + s;
                }
            }
            for(int i = 1; i <= 7;i++)
            {
                string s(1,mem[addr+7][i]);
                load_data_s = load_data_s + s;
            }
            //load_data = (-1) * (mem[addr+7][0]-48) * pow(2,63) + toint(load_data_s,63);
            if(mem[addr+7][0] == '1')
            {
                load_data_s = toIvert(load_data_s,63);
                load_data = -(toint(load_data_s,63) + 1);
            }
            else
            {
                load_data = toint(load_data_s,63);
            }
        }
        if(lsq.lsu_load_store_size == 1 & !lsq.lsu_sign)
        {
            for(int i = 0; i <= 7;i++)
            {
                string s(1,mem[addr][i]);
                load_data_s = load_data_s + s;
            }
            load_data = toint(load_data_s,8);
        }
        if(lsq.lsu_load_store_size == 2 & !lsq.lsu_sign)
        {
            for(int j = 0 ; j <= 1 ;j++)
            {
                for(int i = 0; i <= 7 ;i++)
                {
                    string s(1,mem[addr + j][i]);
                    load_data_s = load_data_s + s;
                }
            }
            load_data = toint(load_data_s,16);
        }
        if(lsq.lsu_load_store_size == 4 & !lsq.lsu_sign)
        {
            for(int j = 0 ; j <= 3 ;j++)
            {
                for(int i = 0; i <= 7 ;i++)
                {
                    string s(1,mem[addr + j][i]);
                    load_data_s = load_data_s + s;
                }
            }
            load_data = toint(load_data_s,32);
        }
    }
    else
    {
        if(lsq.lsu_load_store_size == 1)
        {
            ST_B st_data = toBinary_b(lsq.basic_rs2_data);
            for(int i = 7;i >=0;i--)
            {
                mem[addr][i] = st_data.ins[7-i];
            }
        }
        if(lsq.lsu_load_store_size == 2)
        {
            ST_H st_data = toBinary_h(lsq.basic_rs2_data);
                for(int j = 0 ; j < 2 ; j++)
                {
                    for(int i = 7;i >= 0;i--)
                    {
                        mem[addr][i+j] = st_data.ins[8 * (j+1)-i];
                    }
                }
        }
        if(lsq.lsu_load_store_size == 4)
        {
            ST_W st_data = toBinary_w(lsq.basic_rs2_data);
                for(int j = 0 ; j < 4 ; j++)
                {
                    for(int i = 7;i >= 0;i--)
                    {
                        mem[addr][i+j] = st_data.ins[8 * (j+1)-i];
                    }
                }
        }
        if(lsq.lsu_load_store_size == 8)
        {
            ST_D st_data = toBinary_d(lsq.basic_rs2_data);
                for(int j = 0 ; j < 8 ; j++)
                {
                    for(int i = 7;i >= 0;i--)
                    {
                        mem[addr][i+j] = st_data.ins[8 * (j+1)-i];
                    }
                }
        }
    }
}



//Rename
ROB_LINE_S ins_after_rename;

void Rename(ROB_LINE_S ins_after_decode)
{
    ins_after_rename = ins_after_decode;
    if(ins_after_rename.basic_use_rd & ins_after_rename.basic_rd != 0)
    {
        ins_after_rename.basic_rename_rd = free_list[free_list_head];
        cout << "rd : " << ins_after_rename.basic_rd << "rename rd : " << free_list[free_list_head] << endl;
        ins_after_rename.basic_last_rename_rd = rename_rd[ins_after_rename.basic_rd];
        //rename_rd[ins_after_rename.basic_rd] = free_list[free_list_head];
        free_list_head = (free_list_head + 1) % FREE_LIST;
    }
}

//CSR
ROB_LINE_S csr;
int csr_pc;
void CSR()
{
    csr_req_ready = 0;
    csr_current_cycle = cycle;
    if(csr.basic_ins == 873631859)
    {
        mepc = Read_PRF_data(rename_rd[5]);
    }
    if(csr.basic_ins == 810717299)
    {
        mtvec = Read_PRF_data(rename_rd[5]);
    }
    if(csr.basic_ins == 874524531)
    {
        registers[csr.basic_rename_rd] = mcause;
        registers_p[csr.basic_rename_rd] = 1;
    }
    if(csr.is_mret)
    {
        csr_pc = mepc;
    }
    if(csr.is_ecall)
    {
        csr_pc = mtvec;
    }
}

//Decode

ROB_LINE_S Decode(int decode_pc, long long decode_ins)
{
    
    ROB_LINE_S ins_after_decode;
    INS ins = toBinary(decode_ins);
    
    
    string last_seven_ins;
    string function_three_bit;
    string rs1_s,rs2_s,rd_s;
    int rs1,rs2,rd;
    long long imm_u , imm_j, imm_i, imm_s, imm_b;
    string imm_u_s, imm_j_s, imm_s_s, imm_b_s,imm_i_s;
    for(int i = 6; i >= 0;i--)
    {
        string s(1,ins.ins[i]);
        last_seven_ins = last_seven_ins + s;
    }
    for(int i = 14; i >= 12;i--)
    {
        string s(1,ins.ins[i]);
        function_three_bit = function_three_bit + s;
    }
    for(int i = 11; i >= 7;i--)
    {
        string s(1,ins.ins[i]);
        rd_s = rd_s + s;
    }
    for(int i = 19; i >= 15;i--)
    {
        string s(1,ins.ins[i]);
        rs1_s = rs1_s + s;
    }
    for(int i = 24; i >= 20;i--)
    {
        string s(1,ins.ins[i]);
        rs2_s = rs2_s + s;
    }
    for(int i = 24; i >= 20;i--)
    {
        string s(1,ins.ins[i]);
        rs2_s = rs2_s + s;
    }
    
    
    rd = toint(rd_s,5);
    rs1 = toint(rs1_s,5);
    rs2 = toint(rs2_s,5);
    for(int i = 30; i >= 12;i--)
    {
        string s(1,ins.ins[i]);
        imm_u_s = imm_u_s + s;
    }
    imm_u_s = imm_u_s + "000000000000";
    //imm_u = (-1) * (ins.ins[31]-48) * pow(2,31) + toint(imm_u_s,31);
    if(ins.ins[31] == '1')
    {
        imm_u_s = toIvert(imm_u_s,31);
        imm_u = -(toint(imm_u_s,31) + 1);
    }
    else
    {
        imm_u = toint(imm_u_s,31);
    }
    cout << "imm_u" << imm_u << endl;
    for(int i = 19; i >= 12;i--)
    {
        string s(1,ins.ins[i]);
        imm_j_s = imm_j_s + s;
    }
    string s(1,ins.ins[20]);
    imm_j_s = imm_j_s + s;
    for(int i = 30; i >= 21;i--)
    {
        string s(1,ins.ins[i]);
        imm_j_s = imm_j_s + s;
    }
    imm_j_s = imm_j_s + '0';
    //imm_j = (-1) * (ins.ins[31]-48) * pow(2,20) + toint(imm_j_s,20);
    if(ins.ins[31] == '1')
    {
        imm_j_s = toIvert(imm_j_s,20);
        imm_j = -(toint(imm_j_s,20) + 1);
    }
    else
    {
        imm_j = toint(imm_j_s,20);
    }
    for(int i = 30; i >= 20;i--)
    {
        string s(1,ins.ins[i]);
        imm_i_s = imm_i_s + s;
    }
    
    //imm_i = (-1) * (ins.ins[31]-48) * pow(2,11) + toint(imm_i_s,11);
    if(ins.ins[31] == '1')
    {
        imm_i_s = toIvert(imm_i_s,11);
        imm_i = -(toint(imm_i_s,11) + 1);
    }
    else
    {
        imm_i = toint(imm_i_s,11);
    }
    cout << "imm_i" << imm_i <<endl;
    for(int i = 30; i >= 25;i--)
    {
        string s(1,ins.ins[i]);
        imm_s_s = imm_s_s + s;
    }
    for(int i = 11; i >= 7;i--)
    {
        string s(1,ins.ins[i]);
        imm_s_s = imm_s_s + s;
    }
    //imm_s = (-1) * (ins.ins[31]-48) * pow(2,11) + toint(imm_s_s,11);
    if(ins.ins[31] == '1')
    {
        imm_s_s = toIvert(imm_s_s,11);
        imm_s = -(toint(imm_s_s,11) + 1);
    }
    else
    {
        imm_s = toint(imm_s_s,11);
    }
    string ss(1,ins.ins[7]);
    imm_b_s = imm_b_s + ss;
    for(int i = 30; i >= 25;i--)
    {
        string s(1,ins.ins[i]);
        imm_b_s = imm_b_s + s;
    }
    for(int i = 11; i >= 8;i--)
    {
        string s(1,ins.ins[i]);
        imm_b_s = imm_b_s + s;
    }
    imm_b_s = imm_b_s + '0';
    //imm_b = (-1) * (ins.ins[31]-48) * pow(2,12) + toint(imm_b_s,12);
    if(ins.ins[31] == '1')
    {
        imm_b_s = toIvert(imm_b_s,12);
        imm_b = -(toint(imm_b_s,12) + 1);
    }
    else
    {
        imm_b = toint(imm_b_s,12);
    }
    ins_after_decode.basic_pc = decode_pc;
    ins_after_decode.basic_pc_next = decode_pc + 4;
    ins_after_decode.basic_ins = decode_ins;
    //NOP
    if(decode_ins == 19)
    {
        cout << "decode nop" << endl;
        ins_after_decode.is_nop = 1;
    }
    //CSR
    if(last_seven_ins == "1110011")
    {
        cout << " csr ins" << decode_ins << endl;
        ins_after_decode.is_csr = 1;
        if((decode_ins == 873631859) | (decode_ins == 810717299))
        {
            cout << "decode csr use" << endl;
            ins_after_decode.csr_no_use = 0;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rs1 = 5;
        }
        else if(decode_ins == 874524531)
        {
            cout << "decode csr use" << endl;
            ins_after_decode.csr_no_use = 0;
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_rd = 30;
        }
        else
        {
            cout << "decode csr no use" << endl;
            ins_after_decode.csr_no_use= 1;
        }
    }
    //ECALL
    if(decode_ins == 115)
    {
        cout << "decode ecall" << endl;
        ins_after_decode.is_ecall = 1;
        ins_after_decode.is_csr = 0;
    }
    //MRET
    if(decode_ins == 807403635)
    {
        cout << "decode mret" << endl;
        ins_after_decode.is_mret = 1;
        ins_after_decode.is_csr = 0;
    }
    //FENCE
    if(decode_ins == 267386895)
    {
        cout << "decode fence" << endl;
        ins_after_decode.is_fence = 1;
    }
    //LD
    if(last_seven_ins == "0000011")
    {
        if(function_three_bit == "000")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_load = 1;
            ins_after_decode.lsu_sign = 1;
            ins_after_decode.lsu_load_store_size = 1;
            ins_after_decode.lsu_imm = imm_i;
        }
        if(function_three_bit == "001")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_load = 1;
            ins_after_decode.lsu_sign = 1;
            ins_after_decode.lsu_load_store_size = 2;
            ins_after_decode.lsu_imm = imm_i;
        }
        if(function_three_bit == "010")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_load = 1;
            ins_after_decode.lsu_sign = 1;
            ins_after_decode.lsu_load_store_size = 4;
            ins_after_decode.lsu_imm = imm_i;
        }
        if(function_three_bit == "100")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_load = 1;
            ins_after_decode.lsu_load_store_size = 1;
            ins_after_decode.lsu_imm = imm_i;
        }
        if(function_three_bit == "101")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_load = 1;
            ins_after_decode.lsu_load_store_size = 2;
            ins_after_decode.lsu_imm = imm_i;
        }
        if(function_three_bit == "110")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_load = 1;
            ins_after_decode.lsu_load_store_size = 4;
            ins_after_decode.lsu_imm = imm_i;
        }
        if(function_three_bit == "011")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_load = 1;
            ins_after_decode.lsu_sign = 1;
            ins_after_decode.lsu_load_store_size = 8;
            ins_after_decode.lsu_imm = imm_i;
        }


    }
    //ST
    if(last_seven_ins == "0100011")
    {
        if(function_three_bit == "000")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_use_rs2 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.basic_rs2 = rs2;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_store = 1;
            ins_after_decode.lsu_sign = 1;
            ins_after_decode.lsu_load_store_size = 1;
            ins_after_decode.lsu_imm = imm_s;
        }
        if(function_three_bit == "001")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_use_rs2 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.basic_rs2 = rs2;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_store = 1;
            ins_after_decode.lsu_sign = 1;
            ins_after_decode.lsu_load_store_size = 2;
            ins_after_decode.lsu_imm = imm_s;        
        }
        if(function_three_bit == "010")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_use_rs2 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.basic_rs2 = rs2;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_store = 1;
            ins_after_decode.lsu_sign = 1;
            ins_after_decode.lsu_load_store_size = 4;
            ins_after_decode.lsu_imm = imm_s;        
        }
        if(function_three_bit == "011")
        {
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_use_rs2 = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.basic_rs2 = rs2;
            ins_after_decode.is_lsu = 1;
            ins_after_decode.lsu_store = 1;
            ins_after_decode.lsu_sign = 1;
            ins_after_decode.lsu_load_store_size = 8;
            ins_after_decode.lsu_imm = imm_s;        
        }
    }
    //LUI / LUIPC
    if(last_seven_ins == "0110111")
    {
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.is_lui = 1;
        ins_after_decode.is_alu = 1;
        ins_after_decode.lui_imm = imm_u;
    }
    if(last_seven_ins == "0010111")
    {
        cout << "decode auipc" << endl;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.is_lui = 1;
        ins_after_decode.is_alu = 1;
        ins_after_decode.lui_imm = imm_u + decode_pc;
    }
    // J
    if(last_seven_ins == "1101111")//jal
    {
        cout << "decode jal" << endl;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.is_jump = 1;
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_add = 1;
        ins_after_decode.add_sub_alu_1 = imm_j;
        ins_after_decode.add_sub_alu_2 = decode_pc;
    }
    if(last_seven_ins == "1100111")//jalr
    {
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.is_jump = 1;
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_add = 1;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.add_sub_alu_2 = imm_i;
    }
    //B
    if(last_seven_ins == "1100011")
    {
        cout << "decode branch" << endl;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_use_rd = rd;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_rs2 = rs2;
        ins_after_decode.is_add = 1;
        ins_after_decode.is_branch = 1;
        ins_after_decode.is_alu = 1;
        ins_after_decode.branch_imm = imm_b;
        ins_after_decode.branch_func = function_three_bit;
    }

    //ADD/SUB
    if(last_seven_ins == "0110011" & function_three_bit == "000")//ADD / SUB
    {
        if(ins.ins[30] == '1')
        {
            ins_after_decode.is_alu = 1;
            ins_after_decode.is_sub = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_use_rs2 = 1;
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.basic_rs2 = rs2; 

        }
        else
        {
            ins_after_decode.is_alu = 1;
            ins_after_decode.is_add = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_use_rs2 = 1;
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.basic_rs2 = rs2;
        }
    }
    if(last_seven_ins == "0111011" & function_three_bit == "000")//addw / subw
    {
        if(ins.ins[30] == '1')
        {
            ins_after_decode.is_alu = 1;
            ins_after_decode.is_sub = 1;
            ins_after_decode.basic_is_half = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_use_rs2 = 1;
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.basic_rs2 = rs2; 
        }
        else
        {
            ins_after_decode.is_alu = 1;
            ins_after_decode.is_add = 1;
            ins_after_decode.basic_is_half = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_use_rs2 = 1;
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.basic_rs2 = rs2;
        }
    }
        if(last_seven_ins == "0010011" & function_three_bit == "000" & (decode_ins != 19))//addi
        {
            cout << "decode addi" << endl;
            ins_after_decode.is_alu = 1;
            ins_after_decode.is_add = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.add_sub_alu_2 = imm_i;
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_rd = rd;
        }
        if(last_seven_ins == "0011011" & function_three_bit == "000")//addiw
        {
            cout << "decode addiw" << endl;
            ins_after_decode.is_alu = 1;
            ins_after_decode.is_add = 1;
            ins_after_decode.basic_use_rs1 = 1;
            ins_after_decode.basic_rs1 = rs1;
            ins_after_decode.add_sub_alu_2 = imm_i;
            ins_after_decode.basic_use_rd = 1;
            ins_after_decode.basic_rd = rd;
            ins_after_decode.basic_is_half = 1;
        }

    // XOR OR AND
    if(last_seven_ins == "0110011" & function_three_bit == "100")
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_xor = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
    }
    if(last_seven_ins == "0110011" & function_three_bit == "110")
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_or = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
    }
    if(last_seven_ins == "0110011" & function_three_bit == "111")
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_and = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
    }
    if(last_seven_ins == "0010011" & function_three_bit == "100")
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_xor = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.op_imm = imm_i;
    }
    if(last_seven_ins == "0010011" & function_three_bit == "110")
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_or = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.op_imm = imm_i;
    }
    if(last_seven_ins == "0010011" & function_three_bit == "111")
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_and = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.op_imm = imm_i;
    }
    //SHIFT
    if(last_seven_ins == "0110011" & function_three_bit == "001" & ins.ins[30] == '0')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 0;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
    }
    if(last_seven_ins == "0110011" & function_three_bit == "101" & ins.ins[30] == '0')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
    }
    if(last_seven_ins == "0110011" & function_three_bit == "101" & ins.ins[30] == '1')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 2;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
    }
    if(last_seven_ins == "0010011" & function_three_bit == "001" & ins.ins[30] == '0')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 0;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.shift = rs2;
    }
    if(last_seven_ins == "0010011" & function_three_bit == "101" & ins.ins[30] == '0')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.shift = rs2;
    }
    if(last_seven_ins == "0010011" & function_three_bit == "101" & ins.ins[30] == '1')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 2;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.shift = rs2;
    }
    if(last_seven_ins == "0111011" & function_three_bit == "001" & ins.ins[30] == '0')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 0;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
        ins_after_decode.basic_is_half = 1;
    }
    if(last_seven_ins == "0111011" & function_three_bit == "101" & ins.ins[30] == '0')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
        ins_after_decode.basic_is_half = 1;
    }
    if(last_seven_ins == "0111011" & function_three_bit == "101" & ins.ins[30] == '1')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 2;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
        ins_after_decode.basic_is_half = 1;
    }
    if(last_seven_ins == "0011011" & function_three_bit == "001" & ins.ins[30] == '0')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 0;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.shift = rs2;
        ins_after_decode.basic_is_half = 1;
    }
    if(last_seven_ins == "0011011" & function_three_bit == "101" & ins.ins[30] == '0')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.shift = rs2;
        ins_after_decode.basic_is_half = 1;
    }
    if(last_seven_ins == "0011011" & function_three_bit == "101" & ins.ins[30] == '1')
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_shift = 1;
        ins_after_decode.shift_type = 2;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.shift = rs2;
        ins_after_decode.basic_is_half = 1;
    }
    //SLT
    if(last_seven_ins == "0110011")
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_slt = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.basic_use_rs2 = 1;
        ins_after_decode.basic_rs2 = rs2;
    }
    if(last_seven_ins == "0010011" & !(decode_ins == 19))
    {
        ins_after_decode.is_alu = 1;
        ins_after_decode.is_slt = 1;
        ins_after_decode.basic_use_rd = 1;
        ins_after_decode.basic_rd = rd;
        ins_after_decode.basic_use_rs1 = 1;
        ins_after_decode.basic_rs1 = rs1;
        ins_after_decode.slt_imm = imm_i;
    }
    return ins_after_decode;
}
//ALU
long long alu_current_cycle;
bool alu_ready;
ROB_LINE_S alu;
void ALU()
{   
    alu_ready = 0;
    alu_current_cycle = cycle;
    if(alu.basic_use_rs1)
    {
        alu.basic_rs1_data = Read_PRF_data(rename_rd[alu.basic_rs1]);
        alu.add_sub_alu_1 = alu.basic_rs1_data;
    }
    if(alu.basic_use_rs2)
    {
        alu.basic_rs2_data = Read_PRF_data(rename_rd[alu.basic_rs2]);
        alu.add_sub_alu_2 = alu.basic_rs2_data;
    }

    //AUI
    if(alu.is_lui)
    {
        alu.alu_result = alu.lui_imm;
    }
    //J
    if(alu.is_add & alu.is_jump)
    {
        alu.basic_pc_next = alu.add_sub_alu_1 + alu.add_sub_alu_2;
        alu.j_b_success = 1;
        alu.alu_result = alu.basic_pc + 4;
    }   
    //B
    if(alu.is_add & alu.is_branch)
    {
        if(alu.branch_func == "000")
        {
            if(alu.basic_rs1_data == alu.basic_rs2_data)
            {
                alu.j_b_success = 1;
                alu.basic_pc_next = alu.basic_pc + alu.branch_imm;
            }
        }
        if(alu.branch_func == "001")
        {
            if(alu.basic_rs1_data != alu.basic_rs2_data)
            {
                alu.j_b_success = 1;
                alu.basic_pc_next = alu.basic_pc + alu.branch_imm;
            }
        }
        if(alu.branch_func == "100" | alu.branch_func == "110")
        {
            if(alu.basic_rs1_data < alu.basic_rs2_data)
            {
                alu.j_b_success = 1;
                alu.basic_pc_next = alu.basic_pc + alu.branch_imm;
            }
        }
        if(alu.branch_func == "101" | alu.branch_func == "111")
        {
            if(alu.basic_rs1_data >= alu.basic_rs2_data)
            {
                alu.j_b_success = 1;
                alu.basic_pc_next = alu.basic_pc + alu.branch_imm;
            }
        }
        cout << "rs1" << alu.basic_rs1_data << "rs2" << alu.basic_rs2_data << endl;
        cout << "alu branch b_success is " << alu.j_b_success << endl;
    }
        //ADD

        if(alu.is_add & !alu.is_branch & !alu.is_jump)
        {   
            if(!alu.basic_is_half)
            {
                alu.alu_result = alu.add_sub_alu_1 + alu.add_sub_alu_2;   
            }
            if(alu.basic_is_half)
            {
                alu.alu_result = alu.add_sub_alu_1 + alu.add_sub_alu_2;
                if(alu.alu_result < -2147483648)
                {
                    alu.alu_result = -alu.add_sub_alu_1 + alu.add_sub_alu_2;
                }


            }
        }
        //SUB
        if(alu.is_sub & !alu.is_branch & !alu.is_jump)
        {
            if(!alu.basic_is_half)
            {
                alu.alu_result = alu.add_sub_alu_1 - alu.add_sub_alu_2;   
            }
            if(alu.basic_is_half)
            {
                alu.alu_result = alu.add_sub_alu_1 - alu.add_sub_alu_2;
                
                ST_D add_64;
                add_64 = toBinary_d(alu.alu_result);

                string add_32;
                for(int i = 30; i >= 0;i--)
                {
                    string s(1,add_64.ins[i]);
                    add_32 = add_32 + s;
                }
                //alu.alu_result = (-1) * (add_64.ins[31]-48) * pow(2,31) + toint(add_32,31);
                if(add_64.ins[31] == '1')
                {
                    add_32 = toIvert(add_32,31);
                    alu.alu_result = -(toint(add_32,31) + 1);
                }
                else
                {
                    alu.alu_result = toint(add_32,31);
                }
            }
            
        }
        //AND
        if(alu.is_and == 1)
        {
            if(alu.basic_use_rs2)
            {
                alu.alu_result = alu.basic_rs1_data & alu.basic_rs2_data;
            }
            else
            {
                alu.alu_result = alu.basic_rs1_data & alu.op_imm;
            }
        }
        //OR
        if(alu.is_or == 1)
        {
            if(alu.basic_use_rs2)
            {
                alu.alu_result = alu.basic_rs1_data | alu.basic_rs2_data;
            }
            else
            {
                alu.alu_result = alu.basic_rs1_data | alu.op_imm;
            }
        }
        //XOR
        if(alu.is_xor == 1)
        {
            if(alu.basic_use_rs2)
            {
                alu.alu_result = alu.basic_rs1_data ^ alu.basic_rs2_data;
            }
            else
            {
                alu.alu_result = alu.basic_rs1_data ^ alu.op_imm;
            }
        }
        //SHIFT
        if(alu.is_shift)
        {
            if(alu.shift_type == 0 & alu.basic_use_rs2)
            {
                alu.alu_result = alu.basic_rs1_data << alu.basic_rs2_data;
            }
            if(alu.shift_type == 1 & alu.basic_use_rs2)
            {
                alu.alu_result = (unsigned long long)alu.basic_rs1_data >> alu.basic_rs2_data;
            }
            if(alu.shift_type == 2 & alu.basic_use_rs2)
            {
                alu.alu_result = alu.basic_rs1_data >> alu.basic_rs2_data;
            }
            if(alu.shift_type == 0 & !alu.basic_use_rs2)
            {
                cout << "alu slli" << endl;
                cout << alu.shift << endl;
                alu.alu_result = alu.basic_rs1_data << alu.shift;
            }
            if(alu.shift_type == 1 & !alu.basic_use_rs2)
            {
                alu.alu_result = (unsigned long long)alu.basic_rs1_data >> alu.shift;
            }
            if(alu.shift_type == 2 & !alu.basic_use_rs2)
            {
                alu.alu_result = alu.basic_rs1_data >> alu.shift;
            }
            if(alu.shift_type == 0 & alu.basic_use_rs2 & alu.basic_is_half)
            {
                alu.alu_result = alu.basic_rs1_data << alu.basic_rs2_data;
            }
            if(alu.shift_type == 1 & alu.basic_use_rs2 & alu.basic_is_half)
            {
                alu.alu_result = (unsigned long long)alu.basic_rs1_data >> alu.basic_rs2_data;
            }
            if(alu.shift_type == 2 & alu.basic_use_rs2 & alu.basic_is_half)
            {
                alu.alu_result = alu.basic_rs1_data >> alu.basic_rs2_data;
            }
            if(alu.shift_type == 0 & !alu.basic_use_rs2 & alu.basic_is_half)
            {
                alu.alu_result = alu.basic_rs1_data << alu.shift;
            }
            if(alu.shift_type == 1 & !alu.basic_use_rs2 & alu.basic_is_half)
            {
                alu.alu_result = (unsigned long long)alu.basic_rs1_data >> alu.shift;
            }
            if(alu.shift_type == 2 & !alu.basic_use_rs2 & alu.basic_is_half)
            {
                alu.alu_result = alu.basic_rs1_data >> alu.shift;
            }
            if(alu.basic_is_half)
            {
                ST_D shift_64;
                shift_64 = toBinary_d(alu.alu_result);
                string shift_32;
                for(int i = 30; i >= 0;i--)
                {
                    string s(1,shift_64.ins[i]);
                    shift_32 = shift_32 + s;
                }
                //alu.alu_result = (-1) * (shift_64.ins[31]-48) * pow(2,31) + toint(shift_32,31);
                if(shift_64.ins[31] == '1')
                {
                    shift_32 = toIvert(shift_32,31);
                    alu.alu_result = -(toint(shift_32,31) + 1);
                }
                else
                {
                    alu.alu_result = toint(shift_32,31);
                }
            }
            
        }
        //SLT
        if(alu.is_slt & alu.basic_use_rs2)
        {
            if(alu.basic_rs1_data < alu.basic_rs2_data)
            {
                alu.slt_result = 1;
            }
        }
        if(alu.is_slt & !alu.basic_use_rs2)
        {
            if(alu.basic_rs1_data < alu.slt_imm)
            {
                alu.slt_result = 1;
            }
        }
    
    
}

//prf
void Alu_Update_Prf(int alu_write_prf_addr, long long alu_write_prf_data)
{
    registers[alu_write_prf_addr] = alu_write_prf_data;
    registers_p[alu_write_prf_addr] = 1;
}

void Lsu_Update_Prf(int lsu_write_prf_addr, long long lsu_write_prf_data)
{
    registers[lsu_write_prf_addr] = lsu_write_prf_data;
    registers_p[lsu_write_prf_addr] = 1;
}
void Commit_Update_Prf(int commit_update_prf)
{
    registers_p[commit_update_prf] = 0;
}



///////////////////////////////////
string ins_from_icache;
long long icache_to_ins_buffer;
long long icache_current_cycle;
int ins_pc;
int icache_to_ins_buffer_pc;
void Fetch_INS_FROM_ICACHE(int pc)
{
    icache_req_ready = 0;
    icache_current_cycle = cycle;
    ins_from_icache = "";
    for(int j = pc + 3 ; j >=pc ; j--)
    {
        for(int i = 7 ; i >= 0 ; i--)
        {
            string s(1,mem[j][i]);
            ins_from_icache = ins_from_icache + s;
        }
    }
    ins_pc = pc;
}

///////////////////////////////////
int btb_update_count;
///////////////////////////////////
CHECK_BTB_S check_btb; 
CHECK_BTB_S CHECK_BTB(int pc)
{
    check_btb.btb_hit = 0;
    for(int i = 0 ; i < BTB_SIZE ; i++)
    {
        if(btb_pc[i] == pc)
        {
            check_btb.btb_hit = 1;
            check_btb.btb_pc = btb_pc_next[i];
        }
    }
    return check_btb;
}
///////////////////////////////////

void UPDATE_BTB(int update_pc, int update_pc_target)
{
    bool btb_hit;
    int btb_hit_pos;
    btb_hit = 0;
    btb_hit_pos = 0;
    for(int i = 0 ; i < BTB_SIZE ; i++)
    {
        if(btb_pc[i] == update_pc)
        {
            btb_hit = 1;
            btb_hit_pos = i;
        }
    }
    if(btb_hit == 1)
    {
        btb_pc_next[btb_hit_pos] = update_pc_target;
    }
    else
    {
        btb_pc[btb_update_count%BTB_SIZE] = update_pc;
        btb_pc_next[btb_update_count%BTB_SIZE] = update_pc_target;
        btb_update_count ++ ;
    }
}

///////////////////////////////////
long long icache_ins_buffer_current_cycle;
int iss_prt;
ROB_LINE_S iss_rob_line;
void CHECK_ROB()
{
    if(rob_head_iss == rob_tail)
    {
        rob_empty = 1;
    }
    else
    {
        rob_empty= 0;
    }
    if(((rob_tail + 1) % ROB_DEPTH) == rob_head_commit)
    {
        rob_full = 1;
    }
    else
    {
        rob_full = 0;
    }
}

void IN_ROB(ROB_LINE_S rob_line)
{
    cout << " in rob " << rob_line.basic_pc;
    tostring(rob_line.basic_pc);
    rob_tail = (rob_tail + 1) % ROB_DEPTH;
    rob_line.rob_index = rob_tail;
    rob[rob_tail] = rob_line;
    CHECK_ROB();
}

void COMMIT()
{
    rob[(rob_head_commit + 1) % ROB_DEPTH].finish = 0;
    rob_head_commit = (rob_head_commit + 1) % ROB_DEPTH;
    CHECK_ROB();
}

void ISSUE()
{
    rob_head_iss = (rob_head_iss + 1) % ROB_DEPTH;
    CHECK_ROB();
}

void ISS_CHECK()
{   
    iss_prt = (rob_head_iss + 1) % ROB_DEPTH;
    iss_rob_line = rob[iss_prt];
    cout << "iss check pc ";
    tostring(iss_rob_line.basic_pc);
    iss_lsu = 0;
    iss_alu = 0;
    commit_directly = 0;
    iss_csr = 0;
    if(iss_rob_line.is_lsu)
    {
        if(iss_rob_line.basic_use_rs1 & (Read_PRF_P(rename_rd[iss_rob_line.basic_rs1]) == 1))
        {
            iss_lsu = 1;
        }
        else
        {
            iss_lsu = 0;
        }
        if(iss_rob_line.basic_use_rs2 & (Read_PRF_P(rename_rd[iss_rob_line.basic_rs2]) == 1))
        {
            iss_lsu = 1;
        }
        else
        {
            iss_lsu = 0;
        }
        if(iss_rob_line.lsu_store)
        {
            if(iss_prt != rob_tail)
            {
                iss_lsu = 0;
            }
        }
        if(!dcache_req_ready)
        {
            iss_lsu = 0;
        }
    }
    if(iss_rob_line.is_alu)
    {
        iss_alu = 1;
        if(iss_rob_line.basic_use_rs1 & Read_PRF_P(rename_rd[iss_rob_line.basic_rs1]) != 1)
        {
            iss_alu = 0;
            cout << "rs1 stop" << rename_rd[iss_rob_line.basic_rs1] << endl;
        }
    
        if(iss_rob_line.basic_use_rs2 & Read_PRF_P(rename_rd[iss_rob_line.basic_rs2]) != 1)
        {
            iss_alu = 0;
            cout << "rs2 stop" << rename_rd[iss_rob_line.basic_rs2] << endl;
        }
       
        if(!alu_ready)
        {
            iss_alu = 0;
        }
    }
    
    if((iss_rob_line.is_csr & iss_rob_line.csr_no_use) | iss_rob_line.is_nop | iss_rob_line.is_fence)
    {   
        cout << "commit_directly" << endl;
        commit_directly = 1;
        rob[(rob_head_iss + 1) % ROB_DEPTH].finish = 1;
    }
    if((iss_rob_line.is_mret) | (iss_rob_line.is_ecall))
    {
        iss_csr = 1;
        commit_directly = 0;
        if(!csr_req_ready)
        {
            iss_csr = 0;
        }
    }
    if(iss_rob_line.is_csr & !iss_rob_line.csr_no_use)
    {
        iss_csr = 1;
        if(iss_rob_line.basic_use_rs1 & Read_PRF_P(rename_rd[iss_rob_line.basic_rs1]) != 1)
        {
            iss_csr = 0;
        }
        if(!csr_req_ready)
        {
            iss_csr = 0;
        }
    }
    if(rob_head_iss == rob_tail)
    {
        iss_alu = 0;
        iss_lsu = 0;
        iss_csr = 0;
    }
}

void CHECK_INS_BUFFER()
{
    if(ins_buffer_tail == ins_buffer_head)
    {
        ins_buffer_empty = 1;
    }
    else
    {
        ins_buffer_empty = 0;
    }
    if(((ins_buffer_tail + 1) % INS_BUFFER_SIZE) == ins_buffer_head)
    {
        ins_buffer_full = 1;
    }
    else
    {
        ins_buffer_full = 0;
    }
}

void WRITE_INS_BUFFER(int ins_pc, long long ins)
{
    ins_buffer_tail = (ins_buffer_tail + 1) % INS_BUFFER_SIZE;
    ins_buffer_pc[ins_buffer_tail] = ins_pc;
    ins_buffer_ins[ins_buffer_tail] = ins;
}
void READ_INS_BUFFER()
{
    ins_buffer_head = (ins_buffer_head + 1) % INS_BUFFER_SIZE;
    ins_buffer_to_decode_pc = ins_buffer_pc[ins_buffer_head];
    ins_buffer_to_decode_ins = ins_buffer_ins[ins_buffer_head];
}

//FLUSH
void FLUSH()
{
    cout << " FLUSH ";
    tostring(rob[lsq.rob_index].basic_pc);
    icache_current_cycle = -10;
    icache_req_ready = 1;
    icache_ins_buffer_current_cycle = -10;
    ins_buffer_head = ins_buffer_tail = 0;
    decode_receive_ins_ready = 1;
    CHECK_INS_BUFFER();
}
void RESET()
{
    for(int i = 0; i < INS_BUFFER_SIZE; i++)
    {
        ins_buffer_pc[i] = 0;
        ins_buffer_ins[i] = 0;
    }
    ins_buffer_full = 0;
    ins_buffer_empty = 1;
    ins_buffer_head = 0;
    ins_buffer_tail = 0;
    icache_req_ready = 1;
    dcache_req_ready = 1;
    csr_req_ready = 1;
    alu_ready = 1;
    icache_ins_buffer_current_cycle = -10;
    icache_current_cycle = -10;
    decode_to_rcu_cycle = -10;
    dcache_current_cycle = -10;
    alu_current_cycle = -10;
    csr_current_cycle = -10;
    decode_receive_ins_ready = 1;
    registers_p[0] = 1;
    registers[0] = 0;
    btb_update_count = 0;
    for(int i = 0 ; i < BTB_SIZE ;i++)
    {
        btb_pc[i] = -1;
        btb_pc_next[i] = 0;
    }

    for(int i = 1; i < REG_SIZE; i++)
    {
        registers[i] = 0;
        registers_p[i] = 0;
    }

    for(int i = 0; i < FREE_LIST;i++)
    {
        free_list[i] = i + 1;
    }

    pc = RESET_VECTOR;
}

void READ_ELF()
{
    ifstream hexfile;
    int ins_line = 0;
    char* buffer = new char[999999];
    hexfile.open("isa/build/hex/rv64ui/add.hex",ios::in);//0-46 47 48 49-95 96 97
    hexfile.seekg(0, std::ios::end);
    int fileLength = hexfile.tellg();
    hexfile.seekg(0, std::ios::beg);
    hexfile.read(buffer,fileLength);
    string a,b;
    long long base = RESET_VECTOR;
    int line_i;
    for(int i = 0 ; i < fileLength ; i++)
    {
        if(i % 49 == 0)
        {
            line_i = 0;
        }
            if(line_i != 48)
            {
                if((line_i % 3) == 0)
                {
                    switch (buffer[i])
                    {
                    case '0':
                        a = "0000";
                        break;
                    case '1':
                        a = "0001";
                        break;
                    case '2':
                        a = "0010";
                        break;
                    case '3':
                        a = "0011";
                        break;
                    case '4':
                        a = "0100";
                        break;
                    case '5':
                        a = "0101";
                        break;
                    case '6':
                        a = "0110";
                        break;
                    case '7':
                        a = "0111";
                        break;
                    case '8':
                        a = "1000";
                        break;
                    case '9':
                        a = "1001";
                        break;
                    case 'A':
                        a = "1010";
                        break;
                    case 'B':
                        a = "1011";
                        break;
                    case 'C':
                        a = "1100";
                        break;
                    case 'D':
                        a = "1101";
                        break;
                    case 'E':
                        a = "1110";
                        break;
                    case 'F':
                        a = "1111";
                        break;    
                    default:
                        cout << "wrong";
                        break;
                    }
                }
                if((line_i % 3) == 1)
                {
                    switch (buffer[i])
                    {
                    case '0':
                        b = "0000";
                        break;
                    case '1':
                        b = "0001";
                        break;
                    case '2':
                        b = "0010";
                        break;
                    case '3':
                        b = "0011";
                        break;
                    case '4':
                        b = "0100";
                        break;
                    case '5':
                        b = "0101";
                        break;
                    case '6':
                        b = "0110";
                        break;
                    case '7':
                        b = "0111";
                        break;
                    case '8':
                        b = "1000";
                        break;
                    case '9':
                        b = "1001";
                        break;
                    case 'A':
                        b = "1010";
                        break;
                    case 'B':
                        b = "1011";
                        break;
                    case 'C':
                        b = "1100";
                        break;
                    case 'D':
                        b = "1101";
                        break;
                    case 'E':
                        b = "1110";
                        break;
                    case 'F':
                        b = "1111";
                        break;    
                    default:
                        cout << "wrong";
                        break;
                    }
                    mem[ins_line][0] = b[3];
                    mem[ins_line][1] = b[2];
                    mem[ins_line][2] = b[1];
                    mem[ins_line][3] = b[0];
                    mem[ins_line][4] = a[3];
                    mem[ins_line][5] = a[2];
                    mem[ins_line][6] = a[1];
                    mem[ins_line][7] = a[0];
                    ins_line += 1;
                }
                line_i = line_i + 1;
            }
    }

}

int main()
{
    cycle = 0;
    READ_ELF();
    RESET();
    for (int i = 0; i < Maxcycle; i++) 
    {
        cout << "cycle ========================" << cycle << endl;
        cout << "prf" << endl;
        for(int i = 0 ; i < REG_SIZE ; i++)
        {
            cout << registers[i] << " " << registers_p[i] << endl; 
        }
        cout << "rename" << endl;
        for(int i = 0 ; i < 33 ; i++)
        {
            cout << i << " " << rename_rd[i] << endl;
        }
        if(rob[(rob_head_commit + 1) % ROB_DEPTH].finish == 1)
        {
            cout << "commit pc ";
            tostring(rob[(rob_head_commit + 1) % ROB_DEPTH].basic_pc);
            COMMIT();
            cout << "after commit" << endl;
            for(int j = 0; j < ROB_DEPTH ;j++)
            {
                cout << j << " " << rob[j].finish << endl;
            }
        }

        if(cycle == icache_current_cycle + ICACHE_DELAY)
        {
            
            cout << "fetch ins from icache " << ins_from_icache << endl;
            icache_to_ins_buffer = toint(ins_from_icache,32);
            icache_ins_buffer_current_cycle = cycle;
            icache_to_ins_buffer_pc = ins_pc; 
            if(ins_pc == 64)
            {
                cout << "pass" << endl;
                return 0;
            }
        }

        if(dcache_current_cycle + DCACHE_DELAY == cycle)
        {
            cout << "lsu done" << endl;
            dcache_req_ready = 1;
            rob[lsq.rob_index].finish = 1;
            if(lsq.basic_use_rd)
            {
                Lsu_Update_Prf(lsq.basic_rename_rd,load_data);
            }
        }
        if(csr_current_cycle + CSR_DELAY == cycle)
        {
            cout << "csr done" << endl;
            csr_req_ready = 1;
            rob[csr.rob_index].finish = 1;
            if(csr.basic_use_rd)
            {
                rename_rd[csr.basic_rd] = csr.basic_rename_rd;
            }
            if(csr.is_ecall | csr.is_mret)
            {
                pc = csr_pc;
                FLUSH();
                decode_receive_ins_ready = 1;
            }
        }
        if(alu_current_cycle + ALU_DELAY == cycle)
        {
            cout << "alu done" << endl;
            alu_ready = 1;
            rob[alu.rob_index].finish = 1;
            if(alu.basic_use_rd)
            {
                if(alu.basic_rd != 0)
                {
                    Alu_Update_Prf(alu.basic_rename_rd,alu.alu_result);
                    rename_rd[alu.basic_rd] = alu.basic_rename_rd;
                }
                if(alu.basic_last_rename_rd != 0)
                {
                    cout << "release free list" << endl;
                    registers_p[alu.basic_last_rename_rd] = 0;
                    free_list[free_list_tail] = alu.basic_last_rename_rd;
                    free_list_tail = (free_list_tail + 1) % FREE_LIST;
                }
            }
            if(alu.is_branch | alu.is_jump)
            {
                if(ins_buffer_pc[(ins_buffer_head + 1) % INS_BUFFER_SIZE] == alu.basic_pc_next)
                {
                    decode_receive_ins_ready = 1;
                }
                else
                {
                    FLUSH();
                    decode_receive_ins_ready = 1;
                    pc = alu.basic_pc_next;
                    UPDATE_BTB(alu.basic_pc,pc);
                }
            }
        }


        
        if(!ins_buffer_full & icache_req_ready)
        {   
            cout << "fetch ins pc ";
            tostring(pc);
            Fetch_INS_FROM_ICACHE(pc);
            check_btb = CHECK_BTB(pc);
            check_btb.btb_hit = 0;
            if(check_btb.btb_hit == 1)
            {
                cout << "btb hit" << endl;
                pc = check_btb.btb_pc;
            }
            else
            {
                pc = pc + 4;
            }
        }
        if(cycle == icache_ins_buffer_current_cycle + ICACHE_INS_BUFFER_DELAY)
        {
            cout << "write ins_buffer pc ";
            tostring(icache_to_ins_buffer_pc);
            icache_req_ready = 1;
            WRITE_INS_BUFFER(icache_to_ins_buffer_pc,icache_to_ins_buffer);
            CHECK_INS_BUFFER();
        }
        if(decode_receive_ins_ready & !ins_buffer_empty)
        {
            decode_to_rcu_cycle = cycle;
            decode_receive_ins_ready = 0;
        }
        if(cycle == decode_to_rcu_cycle + DECODE_RCU_DELAY)
        {
            decode_receive_ins_ready = 1;
            READ_INS_BUFFER();
            CHECK_INS_BUFFER();
            ROB_LINE_S ins_after_decode = Decode(ins_buffer_to_decode_pc,ins_buffer_to_decode_ins);
            Rename(ins_after_decode);
            IN_ROB(ins_after_rename);
            if(rob_full)
            {
                decode_receive_ins_ready = 0;
            }
            if(ins_after_decode.is_jump | ins_after_decode.is_branch | ins_after_decode.is_mret | ins_after_decode.is_ecall)
            {
                decode_receive_ins_ready = 0;
            }
        }
        cout << "rob tail" << rob_tail << endl;
        cout << "rob head iss" << rob_head_iss << endl;
        cout << "rob head commit" << rob_head_commit << endl;
        if(!rob_empty)
        {
            ISS_CHECK();
            if(iss_lsu)
            {   
                lsq = iss_rob_line;
                LSQ_SEND_TO_DCACHE();
                ISSUE();
            }
            if(iss_alu)
            {
                cout << "iss_alu" << endl;
                alu= iss_rob_line;
                ALU();
                ISSUE();
            }
            if(commit_directly)
            {
                cout << "iss directly" << endl;
                ISSUE();
            }
            if(iss_csr)
            {
                cout << "iss csr" << endl;
                csr = iss_rob_line;
                CSR();
                ISSUE();
            }
        }

        cycle++;
    }
}