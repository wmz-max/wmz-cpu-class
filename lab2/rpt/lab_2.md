## cmodel
**1. lab2我所做的内容**

    完成了整个hehe1 core(前段 + 后端) 部分的cmodel，目前可以跑过isa-rv64ui里的指令(用cad4/5的环境)。

**2. 运行方式**   

    vscode 安装如下插件 安装后 点击右上角run code 即可运行 全部代码只有一个文件 main.cpp

![1.png](./images/1.jpg)

**3. 运行结果**

    输出的最后是pass

![2.png](./images/2.jpg)

## Open EDA flow

**report**

    后端的结果在 RUN_2022.12.17_07.35.16 文件中



